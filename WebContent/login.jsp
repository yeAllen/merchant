<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Login</title>
    <style>
    	.login_btn{
    		text-align: center;
            vertical-align: middle;
            cursor: pointer;
            width: 120px;
            height: 40px;
            font-size: 18px;
            line-height: 36px;
            color: #fff;
            font-weight: 400;
            border: 0;
            outline: 0;
            background: #0AE;
            border-radius: 4px;
    	}
        .regist_btn {
            padding: 10px 40px;
            text-decoration: none;
        }
		.text_label{
			font-weight: 400;
            width: 50px;
            border: 0;
            outline: 0;
            border-radius: 4px;
            position: relative;
            padding: 14px 22px;
            line-height: 20px;
            height: 20px;
            color: #fff;
            font-size: 12px;
            vertical-align: middle;
		}
        .text_user_label {
            background: url(${page.request.pageContent}user.png) no-repeat center;
        }
         .text_pwd_label {
            background: url(${page.request.pageContent}pwd.png) no-repeat center;
        }
        .text_input {
            position: relative;
            padding: 9px;
            line-height: 20px;
            height: 20px;
            color: #595959;
            font-size: 12px;
            vertical-align: middle;
            box-shadow: none;
        }
       .login {
    	width: 280px;
    	height:325px;
   		padding: 0 31px 0 33px;
   	 	position: relative;
   	 	overflow: hidden;
   	 	border: 1px solid #a7a7a7;
    }
    .login-modern {
    	border: 1px solid transparent;
    	background-color: #ffffff;

	}
    </style>
</head>

<body style="background: url(${pageContext.request.contextPath}/back.jpg) no-repeat center;">
<div align="center" style="position:fixed;top:14%;left:60%;" class="login login-modern">
    <h1 style="color: #000000">登录</h1>
    <form action="login" method="post">
        <div style="margin-top: 5px;">
            <span class="text_user_label text_label"></span>
            <input type="text" name="username" class="text_input"/>
        </div>
        <div style="margin-top: 13px;">
            <span class="text_pwd_label text_label"></span>
            <input type="password" name="password" class="text_input"/>
        </div>
        <div style="margin-top: 20px">
            <input type="submit" name="login" value="登   陆 " class="login_btn"/>
            <a href="register.jsp" class="regist_btn login_btn">注   册</a>
        </div>
    </form>
</div>
</body>
</html>