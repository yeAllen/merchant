<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Insert title here</title>
    <style type="text/css">
        .text_label {
            margin-top: 20px;
            font-weight: 400;
            border: 0;
            outline: 0;
            background: #0AE;
            border-radius: 4px;
            position: relative;
            padding: 9px;
            line-height: 20px;
            height: 20px;
            color: #fff;
            font-size: 12px;
            vertical-align: middle;
            box-shadow: none;
        }

        .text_input {
            position: relative;
            padding: 9px;
            color: #595959;
            font-size: 12px;
            vertical-align: middle;
            box-shadow: none;
        }

        .regist_btn {
            text-align: center;
            text-decoration: none;
            vertical-align: middle;
            cursor: pointer;
            width: 240px;
            padding: 0 20px;
            font-size: 18px;
            line-height: 36px;
            height: 40px;
            color: #fff;
            font-weight: 400;
            border: 0;
            outline: 0;
            background: #0AE;
            border-radius: 4px;
        }
        
         .register {
        position:fixed;
        top:14%;
        left:60%;
    	width: 280px;
    	height:430px;
   		padding: 0 31px 0 33px;
   	 	overflow: hidden;
   	 	border: 1px solid #a7a7a7;
    }
    .register-modern {
    	border: 1px solid transparent;
    	background-color: #ffffff;
    	filter: progid:DXImageTransform.Microsoft.Gradient(GradientType=0, StartColorStr='#66000000', EndColorStr='#66000000');
	}
    </style>
</head>
<body style="background: url(${pageContext.request.contextPath}/back.jpg) no-repeat center;">
<div class="register register-modern">
    <h1 style="text-align:center; color:#000000;">注册页面</h1>
    <form action="${pageContext.request.contextPath}/regist" method="post" enctype="multipart/form-data" style="width:320px; margin-left:21px; margin-top:10px">
        <div style="margin-top: 15px">
            <span class="text_label">用户名：</span>
            <input type="text" name="name" class="text_input">
        </div>
        <div style="margin-top: 15px">
            <span class="text_label">密&nbsp;&nbsp;&nbsp;&nbsp;码：</span>
            <input type="password" name="password" class="text_input"/>
        </div>
        <div style="margin-top: 15px">
            <span class="text_label">地&nbsp;&nbsp;&nbsp;&nbsp;址：</span>
            <input type="text" name="address" class="text_input"/>
        </div>
        <div style="margin-top: 20px">
            <span class="text_label">身份证：</span>
            <input type="file" name="idCard" class="text_input"/>
        </div>
        <div style="margin-top: 20px">
            <span class="text_label">个性照：</span>
            <input type="file" name="avatar" class="text_input"/>
        </div>
        <div style="margin-top: 15px;">
            <input type="submit" value="  注册  " class="regist_btn"/>
        </div>
    </form>
</div>
</body>
</html>