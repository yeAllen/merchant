<%--
  Created by IntelliJ IDEA.
  User: CHENCO7
  Date: 7/29/2017
  Time: 1:18 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Add Food</title>
    <link rel="stylesheet" href="https://cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <script src="https://cdn.bootcss.com/jquery/3.2.1/jquery.js"></script>

    <script src="https://cdn.bootcss.com/bootstrap/3.3.7/js/bootstrap.min.js"
            integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
            crossorigin="anonymous"></script>
</head>
<body>
<div class="container" style="width: 300px; margin-top: 50px">
    <h1>新增菜品</h1>
    <form action="${pageContext.request.contextPath}/sec/control/addFood" method="post" enctype="multipart/form-data">
        <label>菜品种类:</label>
        <select class="sel" name="categoryId" style="width: 100px; height: 30px;">
            <c:forEach items="${categoriesList}" var="category">
                <option value="${category.id}">${category.type}</option>
            </c:forEach>
        </select><br>

        <div class="form-group">
            <label>菜品名称:</label>
            <input class="form-control" type="text" name="name">
        </div>
        <div class="form-group">
            <label>菜品价格:</label>
            <input class="form-control" type="number" name="price">
        </div>
        <div class="form-group">
            <label>菜品图片:</label>
            <input type="file" name="foodImage">
        </div>
        <button type="submit" class="btn btn-primary">添加</button>
    </form>
</div>
</body>
</html>
