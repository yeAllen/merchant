<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=utf-8"
         pageEncoding="utf-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Insert title here</title>
    <link rel="stylesheet" href="https://cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <script src="https://cdn.bootcss.com/jquery/3.2.1/jquery.js"></script>

    <script src="https://cdn.bootcss.com/bootstrap/3.3.7/js/bootstrap.min.js"
            integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
            crossorigin="anonymous"></script>
</head>
<body>
<div class="container" style="width: 300px; margin-top: 50px">
    <h1>编辑菜品</h1>
    <form action="${pageContext.request.contextPath}/sec/control/editFood" method="post" enctype="multipart/form-data">
        <label>菜品种类:</label>
        <select name="categoryId" style="width: 100px; height: 30px;">
            <c:forEach items="${categoriesList}" var="category">
                <option value="${category.id}">${category.type}</option>
            </c:forEach>
        </select><br>
        <input type="hidden" name="id" value="${food.id}"><br>

        <div class="form-group">
            <label>菜品名称:</label>
            <input class="form-control" type="text" name="name" value="${food.name}">
        </div>

        <div class="form-group">
            <label>菜品价格:</label>
            <input class="form-control" type="number" name="price" value="${food.price}">
        </div>

        <div class="form-group">
            <label>菜品图片:</label>
            <input type="file" name="foodImage">
        </div>

        <button type="submit" class="btn btn-primary">更新</button>
    </form>
</div>

</body>
</html>