<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Insert title here</title>
    <style type="text/css">
        .container {
            width: 70%;
            margin: 0 auto;
            text-align: center;
        }

        .type_div {
            margin-top: 70px;
        }

        th {
            padding: 10px 0;
        }

        td, th {
            border: 1px solid #DDDDDD;
        }

        .type_span {
            background-color: #00AAEE;
            padding: 5px 15px;
            margin-right: 30px;
        }

        .type_span a {
            color: #ffffff;
            text-decoration: none;
        }

        .page {
            padding: 2px;
        }

        .page_a {
            color: #ffffff;
            text-decoration: none;
        }

        .active a {
            background-color: #00AAEE;
        }

        .pagination {
            display: inline-block;
            padding-left: 0;
            margin: 20px 0;
            border-radius: 4px;
        }

        .pagination a {
            position: relative;
            float: left;
            padding: 6px 12px;
            margin-left: -1px;
            line-height: 1.42857143;
            color: #337ab7;
            text-decoration: none;
            border: 1px solid #ddd;
        }

    </style>
</head>
<body>

<div class="container">
    <h1 style="margin-top: 50px">菜品列表</h1>
    <div class="type_div">
        <span value="all" class="type_span"><a
                href="${pageContext.request.contextPath}/sec/control/showFoodList?typeId=all">全部</a></span>
        <c:forEach items="${categoriesList}" var="category">
            <span value="${category.id}" class="type_span"><a
                    href="${pageContext.request.contextPath}/sec/control/showFoodList?typeId=${category.id }">${category.type }</a></span>
        </c:forEach>
        <span class="type_span" style="background-color: #d9534f">
				<a href="${pageContext.request.contextPath}/sec/control/showCategories">添加</a>
			</span>
    </div>
    <table style="width: 100%; margin-top: 50px" cellspacing="0" cellpadding="0">
        <tr>
            <th>名称</th>
            <th>示例图</th>
            <th>价格</th>
            <th>操作</th>
        </tr>
        <c:forEach items="${pageBean.recordList}" var="food">
            <tr>
                <td>${food.name }</td>
                <td><img src="${applicationScope.get("readPath")}/${food.image}" alt="${food.name}" width="50"
                         height="50"/></td>
                <td>${food.price}</td>
                <td style="width: 100px">
                    <a href="${pageContext.request.contextPath}/sec/control/showEditFood?foodId=${food.id}">修改</a>
                    <a href="${pageContext.request.contextPath}/sec/control/deleteFood?foodId=${food.id}&typeId=${param.get('typeId')}">删除</a>
                </td>
            </tr>
        </c:forEach>
    </table>
    <ul class="pagination">
        <c:forEach begin="${pageBean.beginPageIndex}" end="${pageBean.endPageIndex}" var="s">
            <span class="<c:if test="${param.get('curr') == s || (param.get('curr') == null && s == 1)}">active</c:if>">
                <a class="page_a" href="showFoodList?curr=${s}&typeId=${typeId}">${s}</a>
            </span>
        </c:forEach>
    </ul>
</div>

</body>
</html>