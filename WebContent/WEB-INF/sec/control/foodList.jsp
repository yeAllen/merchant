<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=utf-8"
         pageEncoding="utf-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Insert title here</title>
    <script src="https://cdn.bootcss.com/jquery/3.2.1/jquery.js"></script>
    <link rel="stylesheet"
          href="https://cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u"
          crossorigin="anonymous">

    <!-- 最新的 Bootstrap 核心 JavaScript 文件 -->
    <script
            src="https://cdn.bootcss.com/bootstrap/3.3.7/js/bootstrap.min.js"
            integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
            crossorigin="anonymous"></script>
    <style type="text/css">
        .container {
            width: 70%;
            margin: 0 auto;
            text-align: center;
        }

        .type_div {
            margin-top: 70px;
        }

        th {
            padding: 10px 0;
        }

        td, th {
            border: 1px solid #DDDDDD;
        }

        .type_span {
            padding: 5px 15px;
            margin-right: 30px;
        }

        .food_img {
            width: 50px;
            height: 50px;
        }

        .op_td {
            width: 100px;
        }

        #food_table {
            margin-top: 20px;
        }

        #table_title {
            text-align: center;
        }

        .delete_a:hover {
            cursor: pointer
        }

        #update {
            background-color: crimson;
        }
    </style>
</head>
<body>
<!-- 模态框 -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="margin-top:50px">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Modal title</h4>
            </div>
            <div class="modal-body" style="margin:30px;text-align:center;font:lighter;">
                <div id="modal_time"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal" id="modal_cancel">取消</button>
                <button type="submit" class="btn btn-primary" id="modal_sure">确定</button>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <h1 style="margin-top: 50px">菜品列表</h1>
    <div class="type_div" id="categories">
			<span value="all" class="type_span">
				<button type="button" class="btn btn-primary" id="all">全部</button>
			</span>
        <c:forEach items="${categoriesList}" var="category">
				<span value="${category.id}" class="type_span">
					<button type="button" class="btn btn-primary" id="${category.id}">${category.type }</button>
				</span>
        </c:forEach>

        <span class="type_span">
			
			<button class="btn btn-primary" id="update">添加</button>
			</span>
    </div>
    <table id="food_table" class="table table-hover">
        <tr id="table_title">
            <th>名称</th>
            <th>示例图</th>
            <th>价格</th>
            <th>操作</th>
        </tr>
        <c:forEach items="${pageBean.recordList}" var="food">
            <tr>
                <td>${food.name }</td>
                <td><img src="${applicationScope.get("readPath")}/${food.image}" alt="${food.name}" width="50"
                         height="50"/></td>
                <td>${food.price}</td>
                <td style="width: 100px"><a href="${pageContext.request.contextPath}/sec/control/showEditFood?foodId=${food.id}">修改</a>
                    <a class="delete_a" id="${food.id}">删除</a></td>
            </tr>
        </c:forEach>
    </table>

</div>

<script type="text/javascript">
    $(function () {
        var addDelete = function (deleteA) {
            deleteA.on("click", function () {
                $("#modal_sure").click(function () {
                    var id = deleteA.attr("id");
                    $("#myModal").modal('hide');
                    deleteA.parent().parent().remove();
                    $.ajax({
                        type: "GET",
                        url: "http://localhost:8083/merchant/sec/control/deleteFood?foodId=" + id,
                        dataType: "json"
                    }).done(function (msg) {

                    });
                });
                $("#modal_sure").attr("display", "block");
                $(".modal-title").text("删除提示");
                $(".modal-body").text("确定要删除？");
                $("#myModal").modal('show');
            });
        }

        addDelete($(".delete_a"));

        $("#categories button").not("#update").on("click", function () {
            console.log($(this).attr("id"));
            var id = $(this).attr("id");
            $("#food_table tr:gt(0)").empty();
            $.ajax({
                type: "GET",
                url: "http://localhost:8083/merchant/sec/control/showFoodListData?typeId=" + id,
                dataType: "json"
            }).done(function (msg) {
                msg.forEach(function (item) {
                    console.log(item.name);
                    var tr = $("<tr></tr>");
                    $("<td></td>").appendTo(tr).text(item.name);

                    var imgId = $("<td></td>");
                    var img = $("<img/>").addClass("food_img").attr("src", "${applicationScope.get('readPath')}/" + item.image);
                    img.appendTo(imgId);
                    imgId.appendTo(tr);
                    $("<td></td>").appendTo(tr).text(item.price);

                    var opTd = $("<td></td>").addClass("op_td");
                    var updateA = $("<a href='${pageContext.request.contextPath}/sec/control/showEditFood?foodId=" + item.id + "'></a>").text("修改");
                    var deleteA = $("<a></a>").text("删除").addClass("delete_a").attr("id", item.id);
                    updateA.appendTo(opTd);
                    deleteA.appendTo(opTd);
                    opTd.appendTo(tr);

                    tr.appendTo($("#food_table"));
                    addDelete(deleteA);
                });
            });
        });

        var checkStatusTime = setInterval(checkStatus, 2000);
        var i = 3;

        function checkStatus() {
            $.ajax({
                type: "GET",
                url: "http://localhost:8083/merchant/getStatus",
                dataType: "json"
            }).done(function (msg) {

                console.log(msg);
                if (msg.status == "BLACK_LIST") {
                    $("#modal_sure").click(function () {
                        location.href = "http://localhost:8083/merchant/sec/control/showFoodList";
                    });

                    $(".modal-title").text("状态改变通知");
                    $(".modal-body").text("你当前状态以为：" + msg.status);
                    $("#modal_cancel").hide();
                    $("#myModal").modal('show');
                }
                else if (msg.status == "out") {
                    clearInterval(checkStatusTime);
                    $(".modal-title").text("状态改变通知");
                    $(".modal-body").text("服务器已断开,3秒后将跳转页面");
                    $("#modal_cancel").hide();
                    $("#myModal").modal('show');
                    /*  setTimeout(function () {
                     location.href = "http://localhost:8080/merchant/sec/control/showFoodList";
                     }, 3000); */
                    var timehwnd = setInterval(Countdown, 1000);


                }
            });
        }

        function Countdown() {
            console.log(i);
            i--;
            if (i == 0) {
                //clearInterval(timehwnd);
                location.href = "http://localhost:8083/merchant/sec/control/showFoodList";
            } else {
                $("#modal_time").html(i + "秒");
            }
        }

        $("#update").on("click", function () {
            location.href = "${pageContext.request.contextPath}/sec/control/showCategories";
        })

    });

</script>


</body>
</html>