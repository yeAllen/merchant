<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Insert title here</title>
    <script src="https://cdn.bootcss.com/jquery/3.2.1/jquery.js"></script>
    <link rel="stylesheet"
          href="https://cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u"
          crossorigin="anonymous">

    <!-- 最新的 Bootstrap 核心 JavaScript 文件 -->
    <script
            src="https://cdn.bootcss.com/bootstrap/3.3.7/js/bootstrap.min.js"
            integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
            crossorigin="anonymous"></script>
    <style type="text/css">
        .container {
            border: 1px solid #00AAEE;
            width: 60%;
            margin: 50px auto 0;
            padding: 50px;
        }

        td {
            padding-top: 30px;
        }

        .title {
            margin: 100px auto 0;
            width: 60%;
        }
    </style>
</head>
<body>
<!-- 模态框 -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog"
     aria-labelledby="myModalLabel" style="margin-top:50px">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Modal title</h4>
            </div>
            <div class="modal-body" style="margin: 30px;text-align: center;font-weight: lighter"></div>
            <div class="modal-footer">
                <button type="button" id="cancel" class="btn btn-default" data-dismiss="modal">取消</button>
                <button type="submit" class="btn btn-primary" id="modal_login">确定</button>
            </div>
        </div>
    </div>
</div>

<div>
    <div class="title">
        <h1>状态查询</h1>
    </div>
    <div class="container">
        <table style="width: 100%;">
            <tr>
                <th>名称</th>
                <th>地址</th>
                <th>头像</th>
                <th>状态</th>
                <th>消息</th>
                <th>操作</th>
            </tr>
            <tr>
                <td>${user.name}</td>
                <td>${user.address}</td>
                <td><img src="${applicationScope.get("readPath")}/${user.avatar}" style="width: 100px; height: 100px"/>
                </td>
                <td id="status">${statusInfo}</td>
                <td id="message">${message}</td>
                <td>
                    <a href="${pageContext.request.contextPath}/sec/control/showFoodList">刷新</a>
                    <a href="${pageContext.request.contextPath}/sec/reRegisterGoIn" class="reRegist_btn fresh_btn">注册</a>
                </td>
            </tr>
        </table>
    </div>
</div>
<script type="text/javascript">
    $(function () {
        setInterval(checkStatus, 2000);

        function checkStatus() {
            $.ajax({
                type: "GET",
                url: "http://localhost:8083/merchant/getStatus",
                dataType: "json"
            }).done(
                    function (msg) {
                        console.log(msg);
                        if (msg.status != $("#status").text()) {
                            $("#status").text(msg.status);
                            $("#message").text(msg.message);

                            if (msg.status == "WHITE_LIST") {
                                $("#modal_login").click(
                                        function () {
                                            location.href = "http://localhost:8083/merchant/sec/control/showFoodList";
                                        });
                                $(".modal-title").text("状态改变通知");
                                $(".modal-body").text("你的状态已改变为：" + msg.status + "是否跳转");

                                $("#myModal").modal('show');
                            } else {
                                $("#modal_login").click(
                                        function () {
                                            $("#myModal").modal('hide');
                                        });
                                $(".modal-title").text("状态改变通知");
                                $(".modal-body").text(msg.status);
                                $("#myModal").modal('show');
                            }
                        }
                    });
        }

    });
</script>
</body>
</html>