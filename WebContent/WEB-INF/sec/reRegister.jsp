<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Insert title here</title>
    <style type="text/css">
        .text_label {
            border: 0;
            outline: 0;
            background: #0AE;
            border-radius: 4px;
            padding: 9px;
            line-height: 20px;
            height: 20px;
            color: #ffffff;
            font-size: 12px;
            vertical-align: middle;
        }

        .text_input {
            padding: 9px;
            color: #595959;
            font-size: 12px;
            vertical-align: middle;
        }

        .submit_btn {
            cursor: pointer;
            width: 220px;
            padding: 0 20px;
            font-size: 18px;
            line-height: 36px;
            height: 40px;
            color: #fff;
            font-weight: 400;
            border: 0;
            background: #0AE;
            border-radius: 4px;
        }
        .reRegister {
        position:fixed;
        top:25%;
        left:40%;
    	width: 280px;
    	height:400px;
   		padding: 0 31px 0 33px;
   	 	overflow: hidden;
   	 	border: 1px solid #a7a7a7;
    }
    .reRegister-modern {
    	border: 1px solid transparent;
    	background-color: white;
    	_background: 0 0;
    	_border: 0;
    	filter: progid:DXImageTransform.Microsoft.Gradient(GradientType=0, StartColorStr='#66000000', EndColorStr='#66000000');
	}
    </style>
</head>
<body style="background: url(${pageContext.request.contextPath}/back.jpg) no-repeat center;">
<div class="reRegister reRegister-modern">
    <h1 style="text-align:center; color: #000;"> ${user.name }</h1>
    <div >
        <form action="${pageContext.request.contextPath}/sec/reRegister" method="post" enctype="multipart/form-data" style="margin-left:17px; width:400px;">
            <div style="margin-top: 15px;">
                <span class="text_label">密&nbsp;&nbsp;&nbsp;&nbsp;码：</span>
                <input type="password" name="password" class="text_input"/>
            </div>
            <div style="margin-top: 15px;">
                <span class="text_label">地&nbsp;&nbsp;&nbsp;&nbsp;址：</span>
                <input type="text" name="address" class="text_input"/>
            </div>
            <div style="margin-top: 15px;">
                <span  class="text_label">身份证：</span>
                <input type="file" name="idCard" class="text_input"/>
            </div>
            <div style="margin-top: 15px;">
                <span class="text_label">个性照：</span>
                <input type="file" name="avatar" class="text_input"/>
            </div>
            <div style="margin-top: 20px">
                <input type="submit" value="  注册  " class="submit_btn"/>
            </div>
        </form>
    </div>
</div>
</body>
</html>