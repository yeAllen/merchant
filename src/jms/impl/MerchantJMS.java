package jms.impl;

import jms.JMSSender;
import org.apache.activemq.command.ActiveMQQueue;
import util.JMSUtil;

import javax.jms.*;

/**
 * Created by CHENCO7 on 7/28/2017.
 */
public class MerchantJMS implements JMSSender {
    private static final String queueName = "merchant";
    private Destination queue;
    private MessageProducer producer;
    private Session session;
    private Connection connection;

    public MerchantJMS(){
        try {
            connection = JMSUtil.createConnection();
            queue = new ActiveMQQueue(queueName);
            session = connection.createSession(false, Session.CLIENT_ACKNOWLEDGE);
            producer = session.createProducer(queue);
        } catch (JMSException e) {
            e.printStackTrace();
        }
    }

    public void sendMessage(String msg){
        try {
            TextMessage message = session.createTextMessage(msg);
            producer.send(message);
        } catch (JMSException e) {
            e.printStackTrace();
        }
    }

    public void close(){
        try {
            producer.close();
            session.close();
            connection.close();
        } catch (JMSException e) {
            e.printStackTrace();
        }
    }
}
