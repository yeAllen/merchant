package exception;

public class ConfigurationException extends Exception{
  public ConfigurationException(){}
  public ConfigurationException(String msg){
    super(msg);
  }
}