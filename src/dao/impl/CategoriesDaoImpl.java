package dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import pojo.Categories;
import util.DBUtils;
import util.DBUtils;
import dao.CategoriesDao;

public class CategoriesDaoImpl implements CategoriesDao {

	public CategoriesDaoImpl() {
		
	}

	@Override
	public int add(Categories categories) {

		String sql = "insert into categories(id,type) values(?,?)";
		
		PreparedStatement pst = null;
		Connection con = null;
		int m = 0;
		try {
			con=DBUtils.createConnection();
			pst = con.prepareStatement(sql);
			pst.setString(1, categories.getId());
			pst.setString(2, categories.getType());
			m = pst.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} 
		finally {
			DBUtils.close(con, pst, null);
		}
		return m;
	}

	@Override
	public int delete(String id) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int update(Categories t) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public Categories load(String id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Categories> findAll() {

		List<Categories> list=new ArrayList<Categories>();
		String sql = "select * from categories";
		Connection con=null;
		PreparedStatement pst = null;
		ResultSet rs =null;
		try {
			con=DBUtils.createConnection();
			pst = con.prepareStatement(sql);
			rs = pst.executeQuery();
			Categories categories = null;
			while (rs.next()) {
				String id = rs.getString("id");
				String type = rs.getString("type");
				categories = new Categories(id, type);
				list.add(categories);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		finally{
			DBUtils.close(con, pst, rs);
		}
		return list;
	
	}

}
