package dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import pojo.Merchant;
import util.DBUtils;
import dao.MerchantDao;

public class MerchantDaoImpl extends BaseDaoImpl<Merchant> implements MerchantDao {

	@Override
	public int add(Merchant merchant) {
		String sql = "insert into merchants(id,name,address,id_card,avatar,password) values(?,?,?,?,?,?)";
		System.out.println(sql);
		Connection con=null;
		PreparedStatement pst = null;
		con = DBUtils.createConnection();
		int m = 0;
		try {
			con=DBUtils.createConnection();
			pst = con.prepareStatement(sql);
			pst.setString(1, merchant.getId());
			pst.setString(2, merchant.getName());
			pst.setString(3, merchant.getAddress());
			pst.setString(4, merchant.getIdCard());
			pst.setString(5, merchant.getAvatar());
			pst.setString(6, merchant.getPassword());
			m = pst.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} 
		finally {
			DBUtils.close(con, pst, null);
		}
		return m;
		
	}

	public Merchant load(String name ,String psd) {
		String sql = "select * from merchants where name = ? and password = ?";
		Connection con =null;
		PreparedStatement pre = null;
		ResultSet rs = null;
		Merchant merchant =null;
		try {
			con = DBUtils.createConnection();
			pre = con.prepareStatement(sql);
			pre.setString(1, name);
			pre.setString(2, psd);
			rs = pre.executeQuery();
			while(rs.next()){
				merchant = new Merchant();
				merchant.setId(rs.getString("id"));
				merchant.setName(rs.getString("name"));
				merchant.setAddress(rs.getString("address"));
				merchant.setIdCard((rs.getString("id_card")));
				merchant.setAvatar(rs.getString("avatar"));
				merchant.setPassword(rs.getString("password"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}finally{
			DBUtils.close(con, pre, rs);
		}
		return merchant;
	}
	
	@Override
	public int delete(String id) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int update(Merchant merchant) {
		String sql = "update merchants set address = ?, id_card = ?, avatar = ?, password = ? where id = ?";
		System.out.println(sql);
		Connection con=null;
		PreparedStatement pst = null;
		con = DBUtils.createConnection();
		int m = 0;
		try {
			con=DBUtils.createConnection();
			pst = con.prepareStatement(sql);
			pst.setString(1, merchant.getAddress());
			pst.setString(2, merchant.getIdCard());
			pst.setString(3, merchant.getAvatar());
			pst.setString(4, merchant.getPassword());
			pst.setString(5, merchant.getId());
			m = pst.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		finally {
			DBUtils.close(con, pst, null);
		}
		return m;
	}

	@Override
	public Merchant load(String id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Merchant> findAll() {
		// TODO Auto-generated method stub
		return null;
	}

	

}
