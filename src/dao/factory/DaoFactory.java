package dao.factory;

import dao.CategoriesDao;
import dao.FoodsDao;
import dao.MerchantDao;
import dao.impl.CategoriesDaoImpl;
import dao.impl.FoodsDaoImpl;
import dao.impl.MerchantDaoImpl;

//所有dao的工厂方法在这里
public class DaoFactory {
	
	private static MerchantDao merchantDao;
	
	private static FoodsDao foodsDao;
	
	private static CategoriesDao categoriesDao;
	
	public static MerchantDao getMerchantDaoInstance(){
		if (merchantDao==null) {
			merchantDao = new MerchantDaoImpl();
		}
		return merchantDao;
	}
	
	public static FoodsDao getFoodsDaoInstance(){
		if (foodsDao==null) {
			foodsDao = new FoodsDaoImpl();
		}
		return foodsDao;
	}

	public static CategoriesDao getCategoriesDaoInstance(){
		if (categoriesDao==null) {
			categoriesDao = new CategoriesDaoImpl();
		}
		return categoriesDao;
	}


}
