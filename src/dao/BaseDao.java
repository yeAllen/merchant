package dao;

import java.util.List;

public interface BaseDao<T> {
	public int add(T t);
	public int delete(String id);
	public int update(T t);
	public T load(String id);
	public List<T> findAll();

}
