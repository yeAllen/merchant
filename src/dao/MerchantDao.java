package dao;

import pojo.Merchant;

public interface MerchantDao extends BaseDao<Merchant> {
	public Merchant load(String name ,String psd);
}
