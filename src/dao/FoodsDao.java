package dao;

import java.util.List;

import pojo.Food;

public interface FoodsDao extends BaseDao<Food>{

	public List<Food> findByCid(String cId);
	public List<Food> findFoodByCategory(String mId, String typeId,int current, int pageSize);
	public List<Food> findAllByMerchant(String mId,int current, int pageSize);
	public int getCountByCategory(String mId,String typeId) ;
	public int getCountByMerchant(String mId);
	List<Food> findFoodByCategory(String mId, String typeId);
	List<Food> findAllByMerchant(String mId);
	  
}
