
package configuration;

import java.io.*;
import java.util.*;

import configuration.*;
import exception.ConfigurationException;
public class Configuration {
  private Properties config=new Properties();
  private String fn=null;

  public Configuration(){}

  public Configuration(String fileName)
      throws ConfigurationException {
    try {
      FileInputStream fin = new FileInputStream(fileName);
      config.load(fin); 
      fin.close();
    }
    catch (IOException ex) {
    	ex.printStackTrace();
    	/*throw new ConfigurationException
          ("load file error"+fileName);*/
    }
    fn=fileName;
  }

  public void load(BufferedInputStream fin){
	  try {
		config.load(fin);
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} 
  }
  
  public String getValue(String itemName){
    return config.getProperty(itemName);
  }

  public String getValue(String itemName,
                         String defaultValue){
    return config.getProperty(itemName,defaultValue);
  }

  public void setValue(String itemName,String value){
    config.setProperty(itemName,value);
    return;
  }

  public void saveFile(String fileName,String description)
      throws ConfigurationException {
    try {
      FileOutputStream fout
          = new FileOutputStream(fileName);
      config.store(fout, description);
      fout.close();
    }
    catch (IOException ex) {
      throw new ConfigurationException
          ("save file error"+fileName);
    }
  }

  public void saveFile(String fileName)
      throws ConfigurationException {
    saveFile(fileName,"");
  }

  public void saveFile() throws ConfigurationException {
    if(fn.length()==0)
      throw new ConfigurationException
          ("file name is null");
    saveFile(fn);
  }
  
  public static void main(String[] args) throws ConfigurationException {
	  Configuration config = new Configuration("config/default.properties");
	  System.out.println(config.getValue("pageSize")); 
	  System.out.println(config.getValue("nativePath"));
}
}