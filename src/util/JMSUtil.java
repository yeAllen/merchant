package util;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.command.ActiveMQQueue;

import javax.jms.*;

/**
 * Created by CHENCO7 on 7/28/2017.
 */
public class JMSUtil {
    private static final String url = "tcp://10.222.29.172:61616";
    private static final String username = "producer";
    private static final String password = "123456";
    private static ActiveMQConnectionFactory factory;

    static {
            factory = new ActiveMQConnectionFactory(url);
            factory.setUserName(username);
            factory.setPassword(password);
    }

    public static Connection createConnection(){
        Connection connection = null;
        try {
            connection = factory.createConnection();
            connection.start();
        } catch (JMSException e) {
            e.printStackTrace();
        }
        return connection;
    }

}
