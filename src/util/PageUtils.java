package util;

import java.util.List;

import pojo.PageBean;
import configuration.Configuration;
import exception.ConfigurationException;

public class PageUtils {

	
	public static <T> PageBean<T> page(int currentPage,List<T> list,int count){
		Configuration config = null;
		try {
			config = new Configuration("config/default.properties");
			System.out.println(config.getValue("pageSize"));
		} catch (ConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		int pageSize = Integer.parseInt(config.getValue("pageSize"));
		return new PageBean<T>(currentPage, pageSize, list, count);
	}

	public static void main(String[] args) throws ConfigurationException {
		Configuration config = new Configuration("config/default.properties");
		System.out.println(config.getValue("pageSize"));
	}
}
