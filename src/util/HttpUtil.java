package util;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;

/**
 * Created by CHENCO7 on 7/28/2017.
 */
public class HttpUtil {
    private final static String HOST = "http://zha-ita077-w7:8080";

    public static String getResponseStr(String uri) throws IOException {
        byte[] buf = null;
        InputStream in = null;
        HttpURLConnection httpURLConnection = null;

        URL url = new URL(HOST + uri);
        URLConnection connection = url.openConnection();
        httpURLConnection = (HttpURLConnection) connection;
        httpURLConnection.setRequestMethod("GET");
        int len = httpURLConnection.getContentLength();
        in = httpURLConnection.getInputStream();
        buf = new byte[len];
        in.read(buf);

        in.close();
        httpURLConnection.disconnect();

        return new String(buf, "UTF-8");
    }
}
