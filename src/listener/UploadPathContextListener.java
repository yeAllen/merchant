package listener;

import configuration.Configuration;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import java.io.BufferedInputStream;


public class UploadPathContextListener implements ServletContextListener {

    public void contextInitialized(ServletContextEvent event) {
        Configuration config = new Configuration();

        config.load((BufferedInputStream) this.getClass().getClassLoader().getResourceAsStream("default.properties"));
        String readPath = config.getValue("readPath");
        String savePath = config.getValue("savePath");
        int pageSize = Integer.parseInt(config.getValue("pageSize"));
        ServletContext context = event.getServletContext();
        context.setAttribute("readPath", readPath);
        context.setAttribute("savePath", savePath);
        context.setAttribute("pageSize", pageSize);
    }

    @Override
    public void contextDestroyed(ServletContextEvent arg0) {

    }
}
