package service.impl;

import java.util.List;

import dao.CategoriesDao;
import dao.factory.DaoFactory;
import pojo.Categories;
import service.CategoriesManager;

public class CategoriesManagerImpl implements CategoriesManager {

	private CategoriesDao categoriesDao;
	
	public CategoriesManagerImpl() {
		super();
		categoriesDao = DaoFactory.getCategoriesDaoInstance();
	}


	@Override
	public List<Categories> findAll() {
		return categoriesDao.findAll();
	}


	@Override
	public int add(Categories categories) {
		return categoriesDao.add(categories);
	}

}
