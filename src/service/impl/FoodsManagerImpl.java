package service.impl;

import java.util.List;

import dao.FoodsDao;
import dao.factory.DaoFactory;
import pojo.Food;
import service.FoodsManager;

public class FoodsManagerImpl implements FoodsManager {

	private FoodsDao foodsDao;
	
	public FoodsManagerImpl() {
		super();
		foodsDao = DaoFactory.getFoodsDaoInstance();
	}

	@Override
	public List<Food> findByCid(String cId) {
		return foodsDao.findByCid(cId);
	}

	@Override
	public List<Food> findAll() {
		return foodsDao.findAll();
	}

	@Override
	public int add(Food food) {
		// TODO Auto-generated method stub
		return foodsDao.add(food);
	}

	@Override
	public int delete(String id) {
		// TODO Auto-generated method stub
		return foodsDao.delete(id);
	}

	@Override
	public int update(Food food) {
		// TODO Auto-generated method stub
		return foodsDao.update(food);
	}

	@Override
	public Food load(String id) {
		// TODO Auto-generated method stub
		return foodsDao.load(id);
	}

	@Override
	public List<Food> findFoodByCategory(String mId, String typeId, int current, int pageSize) {
		// TODO Auto-generated method stub
		return foodsDao.findFoodByCategory(mId, typeId, current, pageSize);
	}

	@Override
	public List<Food> findFoodByCategory(String mId, String typeId) {
		if(typeId.equals("all")){
			return foodsDao.findAllByMerchant(mId);
		}
		return foodsDao.findFoodByCategory(mId, typeId);
	}
	
	@Override
	public List<Food> findAllByMerchant(String mId, int current, int pageSize) {
		// TODO Auto-generated method stub
		return foodsDao.findAllByMerchant(mId, current, pageSize);
	}

	@Override
	public int getCountByCategory(String mId, String typeId) {
		return foodsDao.getCountByCategory(mId, typeId);
	}

	@Override
	public int getCountByMerchant(String mId) {
		// TODO Auto-generated method stub
		return foodsDao.getCountByMerchant(mId);
	}

}
