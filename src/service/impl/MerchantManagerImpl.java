package service.impl;

import java.util.List;

import pojo.Food;
import pojo.Merchant;
import service.MerchantManager;
import dao.CategoriesDao;
import dao.FoodsDao;
import dao.MerchantDao;
import dao.factory.DaoFactory;

public class MerchantManagerImpl implements MerchantManager {
	
	private MerchantDao merchantDao;

	public MerchantManagerImpl(){
		merchantDao = DaoFactory.getMerchantDaoInstance();
	}
	
	@Override
	public List<Merchant> getAllWarehouses() {
		return merchantDao.findAll();
	}

	@Override
	public Merchant getMerchant(String name, String password) {
		return merchantDao.load(name, password);
	}

	@Override
	public int add(Merchant t) {
		return merchantDao.add(t);
	}

	@Override
	public int update(Merchant t) {
		return merchantDao.update(t);
	}
}
