package service;


import java.util.List;

import pojo.Categories;
import pojo.Food;
import pojo.Merchant;

public interface MerchantManager {
	
	
	public List<Merchant> getAllWarehouses();
	public Merchant getMerchant(String name, String password);
	public int add(Merchant t);
	public int update(Merchant t);
	
	
}
