package service;

import java.util.List;

import pojo.Categories;
import pojo.Food;

public interface CategoriesManager {
	public List<Categories> findAll();
	public int add(Categories categories);
}
