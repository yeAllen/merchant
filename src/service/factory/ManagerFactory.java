package service.factory;

import service.CategoriesManager;
import service.FoodsManager;
import service.MerchantManager;
import service.impl.CategoriesManagerImpl;
import service.impl.FoodsManagerImpl;
import service.impl.MerchantManagerImpl;


//所有manager的工厂方法都在这里
public class ManagerFactory {
	
	private static MerchantManager merchantManager;
	private static FoodsManager foodsManager;
	private static CategoriesManager categoriesManager;
	

	public static MerchantManager getMerchantManagerInstance(){
		if (merchantManager==null) {
			merchantManager = new MerchantManagerImpl();
		}
		return merchantManager;
	}
	
	public static FoodsManager getFoodsManagerInstance(){
		if(foodsManager==null){
			foodsManager = new FoodsManagerImpl();
		}
		return foodsManager;	
	}
	
	public static CategoriesManager getCategoriesManagerInstance(){
		if(categoriesManager==null){
			categoriesManager = new CategoriesManagerImpl();
		}
		return categoriesManager;
	}

}
