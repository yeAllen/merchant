package service;

import java.util.List;

import pojo.Food;

public interface FoodsManager {
	public List<Food> findByCid(String cId);
	public List<Food> findAll();
	public int add(Food food);
	public int delete(String id);
	public int update(Food food);
	public Food load(String id);
	public List<Food> findFoodByCategory(String mId, String typeId,int current, int pageSize);
	public List<Food> findFoodByCategory(String mId, String typeId);
	public List<Food> findAllByMerchant(String mId,int current, int pageSize);
	public int getCountByCategory(String mId,String typeId) ;
	public int getCountByMerchant(String mId);
}
