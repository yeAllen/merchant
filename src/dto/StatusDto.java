package dto;

/**
 * Created by CHENCO7 on 7/29/2017.
 */
public class StatusDto {

    public final static String WAIT_AUDIT="WAIT_AUDIT";
    public final static String SUCCESS="SUCCESS";
    public final static String REJECT="REJECT";
    public final static String BLACK_LIST="BLACK_LIST";
    public final static String WHITE_LIST="WHITE_LIST";

    private String status;
    private String message;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

	@Override
	public String toString() {
		return "StatusDto [status=" + status + ", message=" + message + "]";
	}
    
    
}