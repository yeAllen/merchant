package dto;

/**
 * Created by CHENCO7 on 7/28/2017.
 */
public class Protocol {
    public final static String REGISTER = "register";
    public final static String MODIFY = "modify";

    private String name;
    private String address;
    private String idCard;
    private String avatar;
    private String operater;

    public Protocol() {
    }

    public Protocol(String name, String address, String idCard, String avatar, String operater) {
        this.name = name;
        this.address = address;
        this.idCard = idCard;
        this.avatar = avatar;
        this.operater = operater;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getIdCard() {
        return idCard;
    }

    public void setIdCard(String idCard) {
        this.idCard = idCard;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getOperater() {
        return operater;
    }

    public void setOperater(String operater) {
        this.operater = operater;
    }
}
