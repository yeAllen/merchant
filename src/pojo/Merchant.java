package pojo;

public class Merchant {
    private String id;
    private String name;
    private String password;
    private String address;
    private String idCard;
    private String avatar;

    public Merchant() {

    }

    public Merchant(String id, String name, String password, String address, String idCard, String avatar) {
        this.id = id;
        this.name = name;
        this.password = password;
        this.address = address;
        this.idCard = idCard;
        this.avatar = avatar;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getIdCard() {
        return idCard;
    }

    public void setIdCard(String idCard) {
        this.idCard = idCard;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }
}
