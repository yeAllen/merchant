package pojo;

import configuration.Configuration;
import exception.ConfigurationException;

public class Categories {

	private String id;
	private String type;
	
	public Categories() {
		
	}

	public Categories(String id, String type) {
		super();
		this.id = id;
		this.type = type;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@Override
	public String toString() {
		return "Categories [id=" + id + ", type=" + type + "]";
	}

}
