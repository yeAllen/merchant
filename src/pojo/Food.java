package pojo;

public class Food {

	private String id;
	private String name;
	private double price;
	private String image;
	private String cId;
	private Categories categories;
	private String mId;

	public Food() {
	}

	public Food(String id, String name, double price, String image, String cId, String mId) {
		this.id = id;
		this.name = name;
		this.price = price;
		this.image = image;
		this.cId = cId;
		this.mId = mId;
	}

	public Food(String id, String name, double price, String image, String cId, Categories categories, String mId) {
		super();
		this.id = id;
		this.name = name;
		this.price = price;
		this.image = image;
		this.cId = cId;
		this.categories = categories;
		this.mId = mId;
	}


	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getcId() {
		return cId;
	}

	public void setcId(String cId) {
		this.cId = cId;
	}


	public Categories getCategories() {
		return categories;
	}

	public void setCategories(Categories categories) {
		this.categories = categories;
	}

	public String getmId() {
		return mId;
	}

	public void setmId(String mId) {
		this.mId = mId;
	}

	@Override
	public String toString() {
		return "Food [id=" + id + ", name=" + name + ", price=" + price + ", image=" + image + ", cId=" + cId
				+ ", categories=" + categories + ", mId=" + mId + "]";
	}
}
