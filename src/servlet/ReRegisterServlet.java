package servlet;

import dto.Protocol;
import jms.impl.MerchantJMS;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import pojo.Merchant;
import service.MerchantManager;
import service.factory.ManagerFactory;
import util.FileUtil;
import util.JsonUtil;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.List;

public class ReRegisterServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
    private MerchantManager manager;

    public ReRegisterServlet() {
        manager = ManagerFactory.getMerchantManagerInstance();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        Merchant merchant = (Merchant) session.getAttribute("user");

        String name = merchant.getName();
        String address = "";
        String base64IdCard = "";
        String base64Avatar = "";
        String idCardPath = "";
        String avatarPath = "";
        String realPath = getServletContext().getRealPath("/");

        DiskFileItemFactory factory = new DiskFileItemFactory(1024 * 1024 * 1024, new File("D:\\bin\\temp"));
        ServletFileUpload upload = new ServletFileUpload(factory);
        upload.setFileSizeMax(1024 * 1024 * 1024); // 50k 文件大小

        try {
            List<FileItem> itemList = upload.parseRequest(request);
            for (FileItem item : itemList) {
                if (item.isFormField()) {
                    System.out.println(item.getFieldName() + " ... " + item.getString());
                    if ("password".equals(item.getFieldName())) {
                        merchant.setPassword(item.getString("utf-8"));
                    }
                    if ("address".equals(item.getFieldName())) {
                        address = item.getString("utf-8");
                        merchant.setAddress(address);
                    }
                } else {
                    if (item.getFieldName().equals("avatar")) {
                        avatarPath = new Date().getTime() + item.getName();
                        String upLoadPath = getServletContext().getAttribute("savePath").toString() + "/" + avatarPath;
                        base64Avatar = FileUtil.upload(item.getInputStream(), upLoadPath);
                        merchant.setAvatar(avatarPath);
                    }
                    if (item.getFieldName().equals("idCard")) {
                        idCardPath = new Date().getTime() + item.getName();
                        String upLoadPath = getServletContext().getAttribute("savePath").toString() + "/" + idCardPath;
                        base64IdCard = FileUtil.upload(item.getInputStream(), upLoadPath);
                        merchant.setIdCard(idCardPath);
                    }
                }
            }
        } catch (FileUploadException e) {
            e.printStackTrace();
        }

        Protocol sendProtocol = new Protocol(name, address, base64IdCard, base64Avatar, Protocol.MODIFY);
        if(manager.update(merchant) > 0){
            MerchantJMS merchantJMS = new MerchantJMS();
            String str = JsonUtil.toJsonString(sendProtocol);
            merchantJMS.sendMessage(str);
            response.sendRedirect("../login.jsp");
        }
    }
}
