package servlet;

import dto.Protocol;
import jms.impl.MerchantJMS;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import pojo.Merchant;
import service.MerchantManager;
import service.factory.ManagerFactory;
import util.FileUtil;
import util.JsonUtil;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * Created by CHENCO7 on 7/28/2017.
 */
public class RegistMerchantServlet extends HttpServlet {
    private MerchantManager manager;

    public RegistMerchantServlet() {
        manager = ManagerFactory.getMerchantManagerInstance();
    }

	@Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doPost(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String name = "";
        String address = "";
        String password = "";
        String base64IdCard = "";
        String base64Avatar = "";
        String idCardPath = "";
        String avatarPath = "";
        String realPath = getServletContext().getRealPath("/");

        DiskFileItemFactory factory = new DiskFileItemFactory(1024 * 1024 * 1024, new File("D:\\bin\\temp"));
        ServletFileUpload upload = new ServletFileUpload(factory);
        upload.setFileSizeMax(1024 * 1024 * 1024);

        try {
            List<FileItem> itemList = upload.parseRequest(req);
            for (FileItem item : itemList) {
                if (item.isFormField()) {
                    System.out.println(item.getFieldName() + " ... " + item.getString());
                    if ("name".equals(item.getFieldName())) {
                        name = item.getString("utf-8");
                    }
                    if ("password".equals(item.getFieldName())) {
                        password = item.getString("utf-8");
                    }
                    if ("address".equals(item.getFieldName())) {
                        address = item.getString("utf-8");
                    }
                } else {
                    System.out.println(item.getName() + " ___ " + item.getSize());
                    if (item.getFieldName().equals("avatar")) {
                        avatarPath = new Date().getTime() + item.getName();
                        String upLoadPath = getServletContext().getAttribute("savePath").toString() + "/" + avatarPath;
                        base64Avatar = FileUtil.upload(item.getInputStream(), upLoadPath);
                    }
                    if (item.getFieldName().equals("idCard")) {
                        idCardPath = new Date().getTime() + item.getName();
                        String upLoadPath = getServletContext().getAttribute("savePath").toString() + "/" + idCardPath;
                        base64IdCard = FileUtil.upload(item.getInputStream(), upLoadPath);
                    }
                }
            }
        } catch (FileUploadException e) {
            // 当文件体积过大的时候, 会抛出异常..
            e.printStackTrace();
        }

        Protocol sendProtocol = new Protocol(name, address, base64IdCard, base64Avatar, Protocol.REGISTER);
        Merchant merchant = new Merchant(UUID.randomUUID().toString(), name, password, address, idCardPath, avatarPath);
        if(manager.add(merchant) > 0){
            MerchantJMS merchantJMS = new MerchantJMS();
            String str = JsonUtil.toJsonString(sendProtocol);
            merchantJMS.sendMessage(str);
            resp.sendRedirect("login.jsp");
        }

    }
}
