package servlet;

import dto.StatusDto;
import util.HttpUtil;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import pojo.Merchant;

import java.io.IOException;

public class RefreshServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    public RefreshServlet() {
        super();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String realPath = request.getContextPath();
        response.sendRedirect(realPath + "/sec/control/showFoodList");
    }
}
