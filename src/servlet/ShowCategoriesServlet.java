package servlet;

import pojo.Categories;
import service.CategoriesManager;
import service.factory.ManagerFactory;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * Created by CHENCO7 on 7/29/2017.
 */
public class ShowCategoriesServlet extends HttpServlet {
    private CategoriesManager categoriesManager;

    public ShowCategoriesServlet() {
        categoriesManager = ManagerFactory.getCategoriesManagerInstance();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        List<Categories> categoriesList = categoriesManager.findAll();
        req.setAttribute("categoriesList", categoriesList);
        req.getRequestDispatcher("/WEB-INF/sec/control/addFood.jsp").forward(req, resp);
    }
}
