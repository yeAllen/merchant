package servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import pojo.Categories;
import pojo.Food;
import pojo.Merchant;
import pojo.PageBean;
import service.CategoriesManager;
import service.FoodsManager;
import service.factory.ManagerFactory;
import util.JsonUtil;


public class showFoodListDataServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private FoodsManager foodsManager;
	private CategoriesManager categoriesManager;
	
    public showFoodListDataServlet() {
        super();
        foodsManager = ManagerFactory.getFoodsManagerInstance();
        categoriesManager = ManagerFactory.getCategoriesManagerInstance();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		request.setCharacterEncoding("UTF-8");
		System.out.println("showfooddata");
		HttpSession session = request.getSession();
		Merchant merchant = (Merchant) session.getAttribute("user");
		String mId = merchant.getId();
		List<Food> foodList = new ArrayList<Food>();
		int count = 0;
		String typeId = request.getParameter("typeId") == null ? "all" : request.getParameter("typeId");
		foodList = foodsManager.findFoodByCategory(mId, typeId);
		//request.setAttribute("categoriesList", categoriesList);
		//request.setAttribute("foodList", foodList);
		//request.getRequestDispatcher("/WEB-INF/sec/control/foodList.jsp").forward(request, response);
	System.out.println(typeId);
		if(foodList.size() == 0){
		System.out.println("foodlistnull");
	}
	
		PrintWriter pw = response.getWriter();
		pw.write(JsonUtil.toJsonString(foodList));
		
	}

}
