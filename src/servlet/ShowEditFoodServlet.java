package servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import pojo.Categories;
import pojo.Food;
import pojo.Merchant;
import service.CategoriesManager;
import service.FoodsManager;
import service.factory.ManagerFactory;


public class ShowEditFoodServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
    private FoodsManager foodsManager;
	private CategoriesManager categoriesManager;

    public ShowEditFoodServlet() {
    	 foodsManager = ManagerFactory.getFoodsManagerInstance();
         categoriesManager = ManagerFactory.getCategoriesManagerInstance();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		   Merchant merchant = (Merchant) request.getSession().getAttribute("user");
	        String id = request.getParameter("foodId");
	        Food food = foodsManager.load(id);

	        request.setAttribute("food", food);
	        List<Categories> categoriesList = categoriesManager.findAll();
	        request.setAttribute("categoriesList", categoriesList);
	        request.getRequestDispatcher("/WEB-INF/sec/control/showEditFood.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
