package servlet;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import configuration.Configuration;
import pojo.Food;
import pojo.Merchant;
import service.CategoriesManager;
import service.FoodsManager;
import service.factory.ManagerFactory;
import util.FileUtil;

/**
 * Created by CHENCO7 on 7/29/2017.
 */
public class AddFoodServlet extends HttpServlet {
    private FoodsManager foodsManager;
    private CategoriesManager categoriesManager;

    public AddFoodServlet() {
        foodsManager = ManagerFactory.getFoodsManagerInstance();
        categoriesManager = ManagerFactory.getCategoriesManagerInstance();
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String contextPath = req.getContextPath();
        Merchant merchant = (Merchant) req.getSession().getAttribute("user");
        String name = "";
        String price = "";
        String foodImagePath = "";
        String categoryId = "";
        String realPath = getServletContext().getRealPath("/");

        DiskFileItemFactory factory = new DiskFileItemFactory(1024 * 1024 * 1024, new File("D:\\bin\\temp"));
        ServletFileUpload upload = new ServletFileUpload(factory);
        upload.setFileSizeMax(1024 * 1024 * 1024); // 50k 文件大小

        try {
            List<FileItem> itemList = upload.parseRequest(req);
            for (FileItem item : itemList) {
                if (item.isFormField()) {
                    System.out.println(item.getFieldName() + " ... " + item.getString());
                    if ("name".equals(item.getFieldName())) {
                        name = item.getString("utf-8");
                    }
                    if ("price".equals(item.getFieldName())) {
                        price = item.getString("utf-8");
                    }
                    if ("categoryId".equals(item.getFieldName())) {
                        categoryId = item.getString("utf-8");
                    }
                } else {
                    System.out.println(item.getName() + " ___ " + item.getSize());
                    if (item.getFieldName().equals("foodImage")) {
                        foodImagePath = new Date().getTime() + item.getName();
                        String readPath = getServletContext().getAttribute("savePath").toString() + "/" + foodImagePath;
                        FileUtil.uploadWithOutBase64(item.getInputStream(), readPath);
                    }
                }
            }
        } catch (FileUploadException e) {
            // 当文件体积过大的时候, 会抛出异常..
            e.printStackTrace();
        }

        Double foodPrice = Double.valueOf(price);
        Food food = new Food(UUID.randomUUID().toString(), name, foodPrice, foodImagePath, categoryId, merchant.getId());
        foodsManager.add(food);
        resp.sendRedirect(contextPath + "/sec/control/showFoodList");
    }
}
