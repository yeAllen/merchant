package servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dto.StatusDto;
import pojo.Merchant;
import util.HttpUtil;
import util.JsonUtil;


public class GetStatusServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public GetStatusServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");
		response.setHeader("Content-type", "application/json; charset=UTF-8");
		Merchant merchant = (Merchant) request.getSession().getAttribute("user");
		PrintWriter pw = response.getWriter();
        String name = merchant.getName();
        String status = null;
        StatusDto statusDto = new StatusDto();
        try {
        	status = HttpUtil.getResponseStr("/admins/m/status/get?name=" + name);
		} catch (Exception e) {
			statusDto.setStatus("out");
	        String statusjson = JsonUtil.toJsonString(statusDto);
			pw.write(statusjson);
			return;
		}
      //  System.out.println(statusDto.toString());
        statusDto = (StatusDto) JsonUtil.toJsonObject(status, StatusDto.class);
		System.out.println(statusDto);
		String statusjson = JsonUtil.toJsonString(statusDto);
		System.out.println(statusjson);
        pw.write(statusjson);
	}

}
