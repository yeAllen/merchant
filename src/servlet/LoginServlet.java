package servlet;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import pojo.Merchant;
import service.MerchantManager;
import service.factory.ManagerFactory;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private final static Logger logger = LogManager.getLogger(LoginServlet.class);
	private MerchantManager manager;

	public LoginServlet() {
		manager = ManagerFactory.getMerchantManagerInstance();
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String username = request.getParameter("username");
		String password = request.getParameter("password");
		
		Merchant merchant = manager.getMerchant(username, password);
		if (merchant != null) {
			request.getSession().setAttribute("user", merchant);
//			request.getRequestDispatcher("sec/control/showList").forward(request, response);
			response.sendRedirect("sec/control/showFoodList");
		} else {
			response.sendRedirect("login.jsp");
		}
	}

}
