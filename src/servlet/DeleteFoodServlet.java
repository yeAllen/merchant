package servlet;

import service.FoodsManager;
import service.factory.ManagerFactory;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class DeleteFoodServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static FoodsManager foodsManager;

    public DeleteFoodServlet() {
        foodsManager = ManagerFactory.getFoodsManagerInstance();
    }

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		doPost(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		System.out.println("delete");
		String foodId = request.getParameter("foodId");
		foodsManager.delete(foodId);
	}
}
