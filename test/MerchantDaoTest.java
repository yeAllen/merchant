import java.util.UUID;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import dao.MerchantDao;
import dao.impl.MerchantDaoImpl;
import pojo.Merchant;
import util.DBUtils;

public class MerchantDaoTest {

	private static MerchantDao merchantDao;
	private static String id;
	
	@BeforeClass
	public static void init(){
		DBUtils.setDBCPBds();
		merchantDao = new MerchantDaoImpl();
		id = UUID.randomUUID().toString();
	}
	
	@Test
	public void addTest(){
		System.out.println(id);
		Merchant merchant = new Merchant(id, "test", "test", "a", "123", "123");
		int m = merchantDao.add(merchant);
		Assert.assertTrue(m>0);
	}
	
	@Test
	public void loadTest(){
		Merchant merchant = merchantDao.load("test","test");
		System.out.println(merchant.toString());
		Assert.assertTrue(merchant!=null);
	}
	
	@Test
	public void updateTest(){
		System.out.println(id);
		Merchant merchant = new Merchant(id, "testupdate", "testupdate", "testupdate", "testupdate", "testupdate");
		int m = merchantDao.update(merchant);
		Assert.assertTrue(m>0);
	}
}
