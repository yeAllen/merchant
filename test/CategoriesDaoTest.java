import java.util.List;
import java.util.UUID;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import dao.CategoriesDao;
import dao.impl.CategoriesDaoImpl;
import pojo.Categories;
import util.DBUtils;

public class CategoriesDaoTest {

	private static CategoriesDao categoriesDao;
	private static String id;
	
	@BeforeClass
	public static void init(){
		DBUtils.setDBCPBds();
		categoriesDao = new CategoriesDaoImpl();
		id = UUID.randomUUID().toString();
	}
	
	@Test
	public void addTest(){
		Categories categories = new Categories(id, "test");
		int m = categoriesDao.add(categories);
		Assert.assertTrue(m>0);
	}
	
	@Test
	public void findAll(){
		List<Categories> list = categoriesDao.findAll();
		Assert.assertTrue(list!=null);
	}
	
}
