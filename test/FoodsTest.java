import java.util.UUID;

import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import dao.FoodsDao;
import dao.impl.FoodsDaoImpl;
import pojo.Food;
import util.DBUtils;

public class FoodsTest {

    private static FoodsDao foodsDao;
    private static String id;
    private static String mId;
    private static String tId;

    @BeforeClass
    public static void init() {
        DBUtils.setDBCPBds();
        foodsDao = new FoodsDaoImpl();
        id = UUID.randomUUID().toString();
        mId = UUID.randomUUID().toString();
        tId = UUID.randomUUID().toString();
    }

    @Test
    public void addTest() {
        System.out.println(id);
        Food food = new Food(id, "test", 111, "img", tId, mId);
        int m = foodsDao.add(food);
        Assert.assertTrue(m > 0);
    }

    @Test
    public void updateTest() {
        System.out.println(id);
        Food food = new Food("6e40a765-358a-48ae-a65f-a0937ee3c448", "testUpdate", 111, "img", tId, mId);
        int m = foodsDao.update(food);
        Assert.assertTrue(m > 0);
    }

    @Test
    public void loadTest() {
        Food food = foodsDao.load("6e40a765-358a-48ae-a65f-a0937ee3c448");
        System.out.println(food.toString());
        Assert.assertTrue(food != null);
    }

    @Test
    public void findAllByMerchantTest() {
        Assert.assertNotNull(foodsDao.findAllByMerchant(mId, 1, 2));
    }

    @Test
    public void findFoodByCategoryTest() {
        Assert.assertNotNull(foodsDao.findFoodByCategory(mId, tId, 1, 2));
    }

    @Test
    public void deleteTest() {
        Assert.assertTrue(foodsDao.delete(id) > 0);
    }
}
